	.file	"huibian.c"
	.text
	.globl	var_prod_ele_opt
	.type	var_prod_ele_opt, @function
var_prod_ele_opt:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$32, %esp
	movl	8(%ebp), %eax
	leal	-1(%eax), %edx
	movl	%edx, -12(%ebp)
	movl	8(%ebp), %edx
	subl	$1, %edx
	movl	%edx, -8(%ebp)
	movl	%eax, %edx
	movl	20(%ebp), %eax
	imull	%edx, %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%edx, %eax
	movl	%eax, -4(%ebp)
	movl	24(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	16(%ebp), %eax
	addl	%edx, %eax
	movl	%eax, -24(%ebp)
	movl	$0, -20(%ebp)
	movl	$0, -16(%ebp)
	jmp	.L2
.L3:
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	-4(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-24(%ebp), %eax
	movl	(%eax), %eax
	imull	%edx, %eax
	addl	%eax, -20(%ebp)
	movl	8(%ebp), %eax
	sall	$2, %eax
	addl	%eax, -24(%ebp)
	addl	$1, -16(%ebp)
.L2:
	movl	-16(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	.L3
	movl	-20(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	var_prod_ele_opt, .-var_prod_ele_opt
	.ident	"GCC: (Ubuntu 5.3.1-14ubuntu2) 5.3.1 20160413"
	.section	.note.GNU-stack,"",@progbits
