#include "head.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#define BUFFERSIZE 4096
int main(int argc,char *argv[])
{
    int fd=open(argv[3],O_RDONLY,0); 
    char ch[BUFFERSIZE];
    int size = read(fd,&ch,BUFFERSIZE);
    close(fd);
    if(strcmp(argv[2], "-tx1")==0){
        tx(ch,size);
    }
    else if(strcmp(argv[2], "-to1")==0){
        to(ch,size);
    }
    else if(strcmp(argv[2], "-td1")==0){
        td(ch,size);
    }
    return 0;
}

