	.file	"client.c"
	.section	.rodata
.LC0:
	.string	"127.0.0.1"
.LC1:
	.string	"socket"
.LC2:
	.string	"connect"
	.align 8
.LC3:
	.string	"connected to server/n \345\256\242\346\210\267\347\253\257IP127.0.0.334/n \346\234\215\345\212\241\345\231\250\345\256\236\347\216\260\350\200\205\345\255\246\345\217\26720155334/n \345\275\223\345\211\215\346\227\266\351\227\2642017.11.8/n"
.LC4:
	.string	"%s"
.LC5:
	.string	"Enter string to send:"
.LC6:
	.string	"quit"
.LC7:
	.string	"received:%s/n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8256, %rsp
	movl	%edi, -8244(%rbp)
	movq	%rsi, -8256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-8224(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -8224(%rbp)
	movl	$.LC0, %edi
	call	inet_addr
	movl	%eax, -8220(%rbp)
	movl	$8000, %edi
	call	htons
	movw	%ax, -8222(%rbp)
	movl	$0, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, -8232(%rbp)
	cmpl	$0, -8232(%rbp)
	jns	.L2
	movl	$.LC1, %edi
	call	perror
	movl	$1, %eax
	jmp	.L8
.L2:
	leaq	-8224(%rbp), %rcx
	movl	-8232(%rbp), %eax
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	connect
	testl	%eax, %eax
	jns	.L4
	movl	$.LC2, %edi
	call	perror
	movl	$1, %eax
	jmp	.L8
.L4:
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	leaq	-8208(%rbp), %rsi
	movl	-8232(%rbp), %eax
	movl	$0, %ecx
	movl	$8192, %edx
	movl	%eax, %edi
	call	recv
	movl	%eax, -8228(%rbp)
	movl	-8228(%rbp), %eax
	cltq
	movb	$48, -8208(%rbp,%rax)
	leaq	-8208(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
.L7:
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	leaq	-8208(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	leaq	-8208(%rbp), %rax
	movl	$.LC6, %esi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L11
	leaq	-8208(%rbp), %rax
	movq	%rax, %rdi
	call	strlen
	movq	%rax, %rdx
	leaq	-8208(%rbp), %rsi
	movl	-8232(%rbp), %eax
	movl	$0, %ecx
	movl	%eax, %edi
	call	send
	movl	%eax, -8228(%rbp)
	leaq	-8208(%rbp), %rsi
	movl	-8232(%rbp), %eax
	movl	$0, %ecx
	movl	$8192, %edx
	movl	%eax, %edi
	call	recv
	movl	%eax, -8228(%rbp)
	movl	-8228(%rbp), %eax
	cltq
	movb	$48, -8208(%rbp,%rax)
	leaq	-8208(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC7, %edi
	movl	$0, %eax
	call	printf
	jmp	.L7
.L11:
	nop
	movl	-8232(%rbp), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$0, %eax
.L8:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L9
	call	__stack_chk_fail
.L9:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.3.1-14ubuntu2) 5.3.1 20160413"
	.section	.note.GNU-stack,"",@progbits
