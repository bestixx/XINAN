void bubble(int *data, int count) { 
	if(count == 0)
	return;
	int i, j;
	int *p, *q; 
	for(i=count-1; i!=0; i--){
		p = data, q = data + 1;
		for(j=0; j!=i; ++j)
		{ 
		if( *p > *q )
		{	 
			int t = *p;*p = *q;
			*q = t;
		} 
		p++, q++;
		}
	}
}
