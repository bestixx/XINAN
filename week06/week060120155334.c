#include <stdio.h>
union node
{
	int num;
	char ch;
};

void big_small(union node p)
{
	p.num=0x12345678;

	if(p.ch==0x78)
	{
		printf("20155334's computer is small-endian.\n");
	}
	else
	{
		printf("20155334's computer is big-endian.\n");
	}
}

int main()
{
	union node p;
	big_small(p);
	return 0;
}
