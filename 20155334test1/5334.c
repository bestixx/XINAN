#include <stdio.h>
#include <stdlib.h>

int main()
{
	char ch;
	FILE *fp1;
	if ((fp1=fopen("na.txt","r"))==NULL)
	{
		printf("ERROR");
		exit(0);
	}

	while((ch=fgetc(fp1))!=EOF)
	{
		printf("%x %c ",ch,ch);
	}
	fclose(fp1);
	return 0;
}
