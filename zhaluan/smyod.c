#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#define M 100

void tx(int fd);
void tc(int fd);
int main(int argc,char *argv[])
{
	int fd;
	if((fd=open(argv[1],O_RDONLY,0))==-1)
		printf("无法读取该文件\n");
	else	
	{
	tc(fd);
	close(fd);
	fd=open(argv[1],O_RDONLY,0);
	tx(fd);
	close(fd);
	}
}

void tc(int fd)
{
        int i, a;
        char ch[M];
        
        printf("原文件输出为：\n");
        while((a=read(fd,ch,16))!=0){
        	for(i=0;i<a;i++)
       	 	{
   	     		if(ch[i]=='\n')
      	       			printf("  \\n");
             		else
              			 printf("%4c",ch[i]);
	        }
               printf("\n");
      	}
}
void tx(int fd)
{
	int i, a;
	char ch[M];

	printf("16进制输出为：\n");
	while((a=read(fd,ch,16))!=0){
		for(i=0;i<a;i++)
		{
			if(ch[i]=='\n')
				printf("  0a");
			else
				printf("%4x",ch[i]);
		}
		printf("\n");
	}
}

