#!/bin/sh

#by rocedu(http://www.cnblogs.com/rocedu/)

echo "//==========Today====================================="
echo "code summary infomation:"
find . -name "*.c" -mtime 0 | xargs cat | grep -v ^$ | wc -l 
echo "documents summary infomation:"
find . -name "*.md" -mtime 0 | xargs cat | grep -v ^$ | wc -l 
echo "commit history:"
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F`
echo ""
echo ""

echo "//==========This Week================================="
echo "code summary infomation:"
find . -name "*.c" -mtime -7| xargs cat | grep -v ^$ | wc -l 
echo "documents summary infomation:"
find . -name "*.md" -mtime -7| xargs cat | grep -v ^$ | wc -l 
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F --date="-1 days"`
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F --date="-2 days"`
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F --date="-3 days"`
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F --date="-4 days"`
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F --date="-5 days"`
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F --date="-6 days"`
git log --pretty=format:"%h - %an,%ci: %s " | grep  `date +%F --date="-7 days"`
echo ""
echo ""

echo "//==========All================================="
echo "code summary infomation:"
find . -name "*.c"| xargs cat | grep -v ^$ | wc -l 
echo "documents summary infomation:"
find . -name "*.md"| xargs cat | grep -v ^$ | wc -l 
echo "commit history:"
git log --pretty=format:"%h - %an,%ci: %s "

